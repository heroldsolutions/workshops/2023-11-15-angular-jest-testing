import { ExampleService } from './example.service';
import { TestBed } from '@angular/core/testing';

describe('ExampleService', () => {
  let service: ExampleService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({});
    service = TestBed.inject(ExampleService);
  });

  it('testFunction()', () => {
    const spyLog = jest.spyOn(console, 'log');

    service.testFunction();

    expect(spyLog).toHaveBeenCalledWith('testFunction called');
  });
});
