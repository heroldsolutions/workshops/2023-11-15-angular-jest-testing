import { CounterComponent } from './counter.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Counter } from './counter';
import { ExampleService } from './example.service';

describe('CounterComponent', () => {
  let component: CounterComponent;
  let fixture: ComponentFixture<CounterComponent>;

  let http: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CounterComponent, HttpClientTestingModule],
    }).compileComponents();
    fixture = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    http = TestBed.inject(HttpTestingController);
  });
  it('constructor()', () => {
    expect(component).toBeTruthy();
    expect(component.counter1).toBeNull();
    expect(component.counter2).toBeNull();
  });

  it('ngOnInit()', () => {
    fixture.detectChanges();

    expect(component.counter1).toBeInstanceOf(Counter);
    expect(component.counter1 instanceof Counter).toBe(true);
    expect(component.counter2).toBeInstanceOf(Counter);
  });

  it('generateOutput()', () => {
    fixture.detectChanges();
    component.generateOutput();

    expect(component.output).toEqual({
      one: 0,
      two: 0,
      three: undefined,
    });
    expect(component.output).toStrictEqual({
      one: 0,
      two: 0,
      // three: undefined,
    });
  });

  it('callExample()', () => {
    const exampleService = TestBed.inject(ExampleService);
    const spyFn = jest
      .spyOn(exampleService, 'testFunction')
      .mockImplementation(() => {});

    component.callExample();

    expect(spyFn).toHaveBeenCalledTimes(1);
  });

  it('httpCall()', () => {
    const spyLog = jest.spyOn(console, 'log');

    component.httpCall();

    const request = http.expectOne('https://give.me.my/data');

    expect(request.request.method).toBe('POST');
    expect(request.request.body).toEqual({
      time: '2023',
    });

    request.flush({
      backend: 'Data',
    });

    expect(spyLog).toHaveBeenCalledWith({
      backend: 'Data',
    });
  });

  it('specialHttp()', () => {
    const spyLog = jest.spyOn(console, 'log');

    component.specialHttp();

    const request = http.expectOne('https://give.me.my/data');

    expect(request.request.method).toBe('POST');
    expect(request.request.body).toEqual({
      time: '2023',
    });

    request.flush({
      backend: 'Data',
    });

    expect(spyLog).toHaveBeenCalledWith({
      backend: 'Data',
    });
  });
});
