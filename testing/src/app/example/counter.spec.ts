import { Counter } from './counter';

describe('Counter', () => {
  let counter: Counter;

  beforeAll(() => {
    // runs once before all tests
  });

  beforeEach(() => {
    // runs before each test case
    counter = new Counter();
  });

  afterEach(() => {
    // runs after each test case
  });

  afterAll(() => {
    // after all test cases finished
  });

  describe('up()', () => {
    it('count up once', () => {
      counter.up();

      expect(counter.count).toBe(1);
    });

    it('count up twice', () => {
      counter.up();
      counter.up();

      expect(counter.count).toBe(2);
    });
  });

  it('down()', () => {
    counter.down();

    expect(counter.count).toBe(-1);
  });

  it('log()', () => {
    // console.log = jest.fn();
    const spyLog = jest.spyOn(console, 'log');

    counter.log();

    expect(spyLog).toHaveBeenCalled();
    expect(spyLog).toHaveBeenCalledTimes(1);
    expect(spyLog).toHaveBeenCalledWith(0);
    expect(spyLog).toHaveBeenLastCalledWith(0);
  });
});
