export class Counter {
  private _count = 0;

  public get count() {
    return this._count;
  }

  public up() {
    this._count++;
  }

  public down() {
    this._count--;
  }

  public log() {
    console.log(this._count);
  }
}
