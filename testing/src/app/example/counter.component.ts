import {
  HttpClient,
  HttpRequest,
  HttpEventType,
  HttpResponse,
} from '@angular/common/http';
import {
  Component,
  ChangeDetectionStrategy,
  inject,
  OnInit,
} from '@angular/core';
import { Counter } from './counter';
import { ExampleService } from './example.service';
import { filter, map, take } from 'rxjs';

@Component({
  selector: `test-counter`,
  standalone: true,
  imports: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `counter`,
  styles: [],
})
export class CounterComponent implements OnInit {
  private readonly http = inject(HttpClient);
  private readonly exampleService = inject(ExampleService);

  public counter1: Counter | null = null;
  public counter2: Counter | null = null;

  public output: {
    one: number;
    two: number;
  } = {
    one: -1,
    two: -1,
  };

  public ngOnInit(): void {
    this.counter1 = new Counter();
    this.counter2 = new Counter();
  }

  public generateOutput(): void {
    if (this.counter1 != null && this.counter2 != null) {
      this.output.one = this.counter1.count;
      this.output.two = this.counter2.count;
    }
  }

  public callExample() {
    this.exampleService.testFunction();
  }

  public httpCall(): void {
    this.http
      .post('https://give.me.my/data', {
        time: '2023',
      })
      .subscribe((data) => {
        console.log(data);
      });
  }

  public specialHttp() {
    const request = new HttpRequest('POST', 'https://give.me.my/data', {
      time: '2023',
    });
    this.http
      .request(request)
      .pipe(
        filter(
          (response): response is HttpResponse<object> =>
            response.type === HttpEventType.Response
        ),
        map((response) => (response as HttpResponse<object>).body),
        take(1)
      )
      .subscribe();
  }
}
