import { count } from './count';

describe('count()', () => {
  it('should count an array', () => {
    // arrange
    const data: string[] = ['data1', 'data2'];

    // act
    const length = count(data);

    // assert
    expect(length).toBe(2);
  });

  it('should return -1 with empty array', () => {
    // // arrange
    // const data: string[] = [];
    // // act
    // const length = count(data);
    // // assert
    // expect(length).toBe(-1);
    expect(count([])).toBe(-1);
  });
});
