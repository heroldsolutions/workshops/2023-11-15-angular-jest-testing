import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ExampleService {
  public testFunction(): void {
    console.log('testFunction called');
  }
}
